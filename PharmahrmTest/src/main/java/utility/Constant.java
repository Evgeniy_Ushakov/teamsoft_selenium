package utility;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created on 04.07.2014.
 */
public class Constant {
    public static final String URL =  "http://test.pharmaonline.in.ua/test/"; // "http://test.pharmahrm.com/migration/";
    public static final String USERNAME = "tester";
    public static final String PASSWORD = "test090";
    public static final String PATH_TEST_DATA = "C://TestSel//src//test//java//testData//";
    public static final String FILE_TEST_DATA = "TestData.xlsx";

    /**
     * Constant for new company form
     */
    public static final String NEW_PHARMACY_NAME = "Аптека Селениум Тест";
    public static final String COMPANY_TYPE = "Аптека";
    public static final String TERRITORY = "Киев";
    public static final String STREET_TYPE = "Улица";
    public static final String STREET_NAME = "Комарова";
    public static final String BUILDING_NUMBER = "3";
    public static final String NEW_HOSPITAL_NAME = "Больница Селениум Тест";

    /**
     * Constant for contact form
     */
    public static final String NEW_CONTACT_LAST_NAME = "Селениум";
    public static final String NEW_CONTACT_NAME = "Тест";
    public static final String NEW_CONTACT_MIDDLE_NAME = "Владленович";
    public static final String CONTACT_FORM_SPECIALIZATION = "Кардиология";
    public static final String CONTACT_FORM_CONTACT_TYPE = "Врач-специалист";



    public String getDate(){
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyy");
        System.out.println(dateFormat.format(date));
        return dateFormat.format(date);
    }
}
