package appModul;

import org.openqa.selenium.WebDriver;
import pageObgects.LogInPagePOM;
import utility.ExcelUtils;

/**
 * Created on 04.07.2014.
 */


public class LogInAction {

    public static void Execute(WebDriver driver) throws Exception{
        String login = ExcelUtils.getCellData(1, 1);
        String password = ExcelUtils.getCellData(1, 2);
        LogInPagePOM.txtbx_UserName(driver).clear();
        LogInPagePOM.txtbx_UserName(driver).sendKeys(login);
        LogInPagePOM.txtbx_Password(driver).clear();
        LogInPagePOM.txtbx_Password(driver).sendKeys(password);
        LogInPagePOM.btn_LogIn(driver).click();
    }
}
