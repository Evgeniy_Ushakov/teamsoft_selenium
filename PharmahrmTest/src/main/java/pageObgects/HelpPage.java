package pageObgects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;


public class HelpPage extends Page{

    @FindBy(how = How.CSS, using = "#help > div.ui-header.ui-bar-a > h1.ui-title")
    public static WebElement title_Help;

    @FindBy (how = How.XPATH, using = "//div[@id='help']/div/a[2]/span/span")
    public static WebElement btn_MainHelp;

    public HelpPage (WebDriver driver) {
        super(driver);
    }

    public void clickBtnMain () throws InterruptedException {
        clickAndWaitForText(btn_MainHelp, HomePage.title_main, "Главная");
    }
}
