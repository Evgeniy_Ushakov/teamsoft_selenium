package pageObgects;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class NewActivePage extends Page{

    @FindBy(how = How.CSS, using = "#new_active > div.ui-header.ui-bar-a > h1.ui-title")
    public static WebElement title_NewActive;

    @FindBy (how = How.XPATH, using = "//div[@id='new_active']/div/a[2]/span")
    public static WebElement btn_MainNewActive;

    public NewActivePage (WebDriver driver) {
        super(driver);
    }

    public void clickBtnMain () throws InterruptedException {
        clickAndWaitForText(btn_MainNewActive, HomePage.title_main, "Главная");
    }

}
