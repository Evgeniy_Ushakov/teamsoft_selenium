package pageObgects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;


public class ReportPage extends Page{

    @FindBy(how = How.CSS, using = "#report > div.ui-header.ui-bar-a > h1.ui-title")
    public static WebElement title_Report;

    @FindBy (how = How.XPATH, using = "//div[@id='report']/div/a[2]/span/span")
    public static WebElement btn_MainReport;

    public ReportPage (WebDriver driver) {
        super(driver);
    }

    public void clickBtnMain () throws InterruptedException {
        clickAndWaitForText(btn_MainReport, HomePage.title_main, "Главная");
    }
}
