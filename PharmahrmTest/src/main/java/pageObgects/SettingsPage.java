package pageObgects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;


public class SettingsPage extends Page{

    @FindBy(how = How.CSS, using = "#settings > div.ui-header.ui-bar-a > h1.ui-title")
    public static WebElement title_Settings;

    @FindBy (how = How.XPATH, using = "//div[@id='settings']/div/a[2]/span/span")
    public static WebElement btn_MainSettings;

    public SettingsPage (WebDriver driver) {
        super(driver);
    }

    public void clickBtnMain () throws InterruptedException {
        clickAndWaitForText(btn_MainSettings, HomePage.title_main, "Главная");
    }
}
