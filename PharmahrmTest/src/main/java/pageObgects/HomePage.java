package pageObgects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import pageObgects.CompanyContactPages.CompanyContactPage;
import pageObgects.ProfilePages.ProfilePage;


public class HomePage extends Page{


    @FindBy (how = How.CSS, using = "h1.ui-title")
    public static WebElement title_main;

    @FindBy (how=How.ID, using = "profile_page")
    public static WebElement btn_Profile;

    @FindBy (how=How.ID, using = "new_active_page")
    public static WebElement btn_NewActive;

    @FindBy (how=How.ID, using = "company_contact_page")
    public static WebElement btn_CompanyContact;

    @FindBy (how=How.ID, using = "report_page")
    public static WebElement btn_Report;

    @FindBy (how=How.ID, using = "organogramma_page")
    public static WebElement btn_Organogramma;

    @FindBy (how=How.ID, using = "presentation_page")
    public static WebElement btn_Presentation;

    @FindBy (how=How.ID, using = "forum")
    public static WebElement btn_Forum;

    @FindBy (how=How.ID, using = "settings_page")
    public static WebElement btn_Settings;

    @FindBy (how=How.ID, using = "help_page")
    public static WebElement btn_Help;

    @FindBy (how=How.XPATH, using = "//div[@id='main']/div[2]/ul/li[10]/div/div/a/h3")
    public static WebElement btn_Exit;


    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void clickBtnProfile ()throws Exception{
        clickAndWaitForText(btn_Profile, ProfilePage.title_profile, "Профиль");
    }

    public void clickBtnNewActive ()throws Exception{
        clickAndWaitForText(btn_NewActive, NewActivePage.title_NewActive, "Новая активность");
    }

    public CompanyContactPage clickBtnCompanyContact ()throws Exception{
        clickAndWaitForText(btn_CompanyContact, CompanyContactPage.title_CompanyContact, CompanyContactPage.TITLE_PAGE_TEXT);
        return new CompanyContactPage(driver);
    }

    public void clickBtnReport ()throws Exception{
        clickAndWaitForText(btn_Report, ReportPage.title_Report, "Отчеты");
    }

    public void clickBtnOrganogramma ()throws Exception{
        clickAndWaitForText(btn_Organogramma, OrganogrammaPage.title_Organogramma, "Органограмма");
    }

    public void clickBtnPresentation ()throws Exception{
        clickAndWaitForText(btn_Presentation, PresentationPage.title_Presentation, "Презентации");
    }

    public void clickBtnForum()throws Exception{
        clickAndWaitForText(btn_Forum, ForumPage.title_Forum, "Разделы форума");
    }

    public void clickBtnSettings ()throws Exception{
        clickAndWaitForText(btn_Settings, SettingsPage.title_Settings, "Настройки");
    }

    public void clickBtnHelp ()throws Exception{
        clickAndWaitForText(btn_Help, HelpPage.title_Help, "Помощь");
    }

    public void clickBtnExit ()throws Exception{
        clickAndWaitForText(btn_Exit, LogInPage.title_login, "Авторизация");
    }
}
