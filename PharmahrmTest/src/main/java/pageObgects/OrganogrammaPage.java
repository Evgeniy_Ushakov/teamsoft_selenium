package pageObgects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by Роман on 21.07.2014.
 */
public class OrganogrammaPage extends Page{

    @FindBy(how = How.CSS, using = "#organogramma > div.ui-header.ui-bar-a > h1.ui-title")
    public static WebElement title_Organogramma;

    @FindBy (how = How.XPATH, using = "//div[@id='organogramma']/div/a[2]/span")
    public static WebElement btn_MainOrganogramma;

    public OrganogrammaPage (WebDriver driver) {
        super(driver);
    }

    public void clickBtnMain () throws InterruptedException {
        clickAndWaitForText(btn_MainOrganogramma, HomePage.title_main, "Главная");
    }
}
