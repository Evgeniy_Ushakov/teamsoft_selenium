package pageObgects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class LogInPage extends Page {

    @FindBy (how = How.CSS, using = "h1.ui-title")
    public static WebElement title_login;

    @FindBy (how = How.NAME, using = "login")
    public WebElement txtbx_userName;

    @FindBy (how = How.NAME,using = "password")
    public WebElement txtbx_password;

    @FindBy (how = How.CSS, using = "input.ui-btn-hidden")
    public WebElement btn_logIn;

    public LogInPage (WebDriver driver){
        super(driver);
   //     PageFactory.initElements(driver, LogInPage.class);
    }


    public void execute(String login, String password) throws Exception{
        txtbx_userName.clear();
        txtbx_userName.sendKeys(login);
        txtbx_password.clear();
        txtbx_password.sendKeys(password);
        btn_logIn.click();
    }

    public HomePage logInSuccess (String login, String password) throws Exception {
        execute(login, password);
        return new HomePage(driver);
    }
}
