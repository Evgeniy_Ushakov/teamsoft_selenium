package pageObgects;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import static org.junit.Assert.*;

public class Page {

    protected WebDriver driver;
    protected PageManager pages;

    public Page(PageManager pages) {
        this.pages = pages;
        driver = pages.getDriver();
    }

    public Page (WebDriver driver) {
        this.driver = driver;
    }

    public void clickAndWaitForText(WebElement clickElement, WebElement assertElement, String assertText) throws InterruptedException {
        Thread.sleep(1000);
        assertTrue(isElementPresent(clickElement));
        clickElement.click();
        for (int second = 0; ; second++) {
            if (second >= 3) fail("timeout");
            try {
                if (isElementPresent(assertElement))
                    break;
            }
            catch (Exception e) {
            }
           //Thread.sleep(2000);
        }
        Thread.sleep(1000);
     //   System.out.println(assertElement.getText());
        assertEquals(assertText, assertElement.getText());
    }

    public void ifElementPresentClickAndWait(WebElement element) throws InterruptedException {
        assertTrue(isElementPresent(element));
        element.click();
        for (int second = 0;; second++) {
            if (second >= 3) fail("timeout");
            try { if (isElementPresent(element))
                break;
            }
            catch (Exception e) {

            }
            Thread.sleep(1000);
        }
    }

    public String saveAttribute (WebElement element){
        assertTrue(isElementPresent(element));
        String value = element.getAttribute("value");
        System.out.println(value);
        return value;
    }

    public String saveText (WebElement element){
        assertTrue(isElementPresent(element));
        String text = element.getText();
        System.out.println(text);
        return text;
    }

    public void selectByTextInDropDownMenu(WebElement element, String text) {
        Select select = new Select(element);
        select.selectByVisibleText(text);
    }

    public boolean isElementPresent(WebElement element) {
        try {
            element.isDisplayed();
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Element not found");
            return false;
        }
    }
    public boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }

    }
}
