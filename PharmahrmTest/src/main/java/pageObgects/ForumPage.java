package pageObgects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;


public class ForumPage extends Page{

    @FindBy(how = How.CSS, using = "#forumsection > div.ui-header.ui-bar-a > h1.ui-title")
    public static WebElement title_Forum;

    @FindBy (how = How.XPATH, using = "//div[@id='forumsection']/div/a[2]/span/span")
    public static WebElement btn_MainForum;

    public ForumPage (WebDriver driver) {
        super(driver);
    }

    public void clickBtnMain () throws InterruptedException {
        clickAndWaitForText(btn_MainForum, HomePage.title_main, "Главная");
    }
}
