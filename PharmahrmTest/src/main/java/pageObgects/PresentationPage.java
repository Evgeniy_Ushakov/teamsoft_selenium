package pageObgects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;


public class PresentationPage extends Page{

    @FindBy(how = How.CSS, using = "#presentation > div.ui-header.ui-bar-a > h1.ui-title")
    public static WebElement title_Presentation;

    @FindBy (how = How.XPATH, using = "//div[@id='presentation']/div/a[2]/span/span")
    public static WebElement btn_MainPresentation;

    public PresentationPage (WebDriver driver) {
        super(driver);
    }

    public void clickBtnMain () throws InterruptedException {
        clickAndWaitForText(btn_MainPresentation, HomePage.title_main, "Главная");
    }
}
