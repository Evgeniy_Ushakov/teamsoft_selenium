package pageObgects;


import org.openqa.selenium.WebDriver;
import pageObgects.CompanyContactPages.TerritorySearchPage;

public class PageManager {

    private WebDriver driver;

    protected TerritorySearchPage searchTerritoryPage;

    public PageManager (WebDriver driver){
        this.driver = driver;
  //      searchTerritoryPage = initElement(new SearchTerritoryPage(this));
    }

    private <T extends Page> T initElement (T page) {
     //   PageFactory.initElements(driver, page);
        return page;
    }

    public WebDriver getDriver (){
        return driver;
    }
}
