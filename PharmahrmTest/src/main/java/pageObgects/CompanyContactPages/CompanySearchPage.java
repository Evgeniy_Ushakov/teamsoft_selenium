package pageObgects.CompanyContactPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import pageObgects.Page;

import static org.junit.Assert.assertTrue;

public class CompanySearchPage extends Page{

    // title
    public static final String TITLE_PAGE_TEXT = "Учреждение";

    @FindBy(how = How.CSS, using = "#company > div.ui-header.ui-bar-a > h1.ui-title")
    public static WebElement title_newCompany;

    // Filed of company page

    @FindBy(how = How.ID, using = "company_search")
    public static WebElement field_companySearch;

    // Buttons of company search page

    @FindBy(how = How.ID, using = "company_search_button")  // //a[@id='company_search_button']/span  (xpath)
    public static WebElement btn_search;

    // Company list
    @FindBy(how = How.XPATH, using = "//ul[@id='company_list']/li/div/div/a/h4")
    public static WebElement firstCompanyInList;

    public CompanySearchPage (WebDriver driver){
        super(driver);
    }

    public void setFieldValue(String companySearch) throws InterruptedException {
        field_companySearch.sendKeys(companySearch);
        btn_search.click();
        assertTrue(isElementPresent(firstCompanyInList));
        saveText(firstCompanyInList);
    }

    public NewContactPage chooseCompany () throws InterruptedException {
        clickAndWaitForText(firstCompanyInList, NewContactPage.title_newContact, NewContactPage.NEW_CONTACT_TITLE_TEXT);
        return new NewContactPage(driver);
    }
}
