package pageObgects.CompanyContactPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import pageObgects.Page;

/**
 * Created on 12.08.2014.
 */
public class NewCompanyPage extends Page{

    // Title
    public static final String TITLE_PAGE_TEXT = "Новое учреждение";

    @FindBy(how = How.CSS, using = "#companyform > div.ui-header.ui-bar-a > h1.ui-title")
    public static WebElement title_companyForm;

    // alert message

    @FindBy (how = How.XPATH, using = "//div[@id='companyform']/div[3]/div")
    public static WebElement title_alertMessage;       //  "Сообщение"

    // Filled of new company form

    @FindBy(how = How.ID, using = "companyform_name")
    public static WebElement field_companyName;

    @FindBy(how = How.ID, using = "companyform_territory")
    public static WebElement field_territory;

    @FindBy(how = How.ID, using = "companyform_street")
    public static WebElement field_street;

    @FindBy(how = How.ID, using = "companyform_building")
    public static WebElement field_building;

    @FindBy(how = How.ID, using = "companyform_postcode")
    public static WebElement field_postcode;

    @FindBy(how = How.ID, using = "companyform_phone1")
    public static WebElement field_phone1;

    @FindBy(how = How.ID, using = "companyform_fax")
    public static WebElement field_fax;

    @FindBy(how = How.ID, using = "companyform_email")
    public static WebElement field_email;

    @FindBy(how = How.ID, using = "companyform_site")
    public static WebElement field_site;

    @FindBy(how = How.ID, using = "companyform_tovarooborot")
    public static WebElement field_tovarooborot;

    @FindBy(how = How.ID, using = "companyform_Callpressure1")
    public static WebElement field_сallPressure1;

    // Autocompleted field of company form

    @FindBy(how = How.ID, using = "companyform_addinfo5")
    public static WebElement field_addInfo5;               // Признак

    @FindBy(how = How.ID, using = "companyform_maincompany")
    public static WebElement field_mainCompany;

    @FindBy(how = How.ID, using = "companyform_statusverification")
    public static WebElement field_statusVerification;

    // Drop-down menu of company form

    @FindBy(how = How.ID, using = "companyform_companytype_id")
    public static WebElement menu_companyType;

    @FindBy(how = How.ID, using = "companyform_category_id")
    public static WebElement menu_category;

    @FindBy(how = How.ID, using = "companyform_streettype_id")
    public static WebElement menu_streetType;

    @FindBy(how = How.ID, using = "companyform_archivereasons_id")
    public static WebElement menu_archiveReasons;

    //Button of company form

    @FindBy(how = How.ID, using = "companyform_search_button")
    public static WebElement btn_search;

    @FindBy(how = How.XPATH, using = "//div[@id='companyform']/div/a[2]/span/span")
    public static WebElement btn_mainCompanyForm;

    @FindBy(how = How.XPATH, using = "//div[@id='companyform']/div/a/span")
    public static WebElement btn_backCompanyForm;

    @FindBy(how = How.ID, using = "companyform_staff_page")
    public static WebElement btn_staff;

    @FindBy(how = How.ID, using = "companyform_responsible_page")
    public static WebElement btn_responsible;

    @FindBy(how = How.ID, using = "companyform_showonmap_page")
    public static WebElement btn_showOnMap;

    @FindBy(how = How.ID, using = "companyform_sales_page")
    public static WebElement btn_sales;

    @FindBy(how = How.ID, using = "companyform_history_page")
    public static WebElement btn_history;

    @FindBy(how = How.ID, using = "companyform_share_page")
    public static WebElement btn_share;                        // Акции

    @FindBy(how = How.ID, using = "companyform_block_newtask")
    public static WebElement btn_newTask;

    @FindBy(how = How.ID, using = "companyform_block_aptfile")
    public static WebElement btn_aptFile;

    @FindBy(how = How.XPATH, using = "//div[@id='companyform']/div[2]/div[18]/div/span")
    public static WebElement btn_save;


    public NewCompanyPage(WebDriver driver) {
        super(driver);
    }

    public void setCompanyName (String newCompanyName) throws InterruptedException {
        field_companyName.sendKeys(newCompanyName);
    }

    public void setCompanyType (String companyType) throws InterruptedException {
        menu_companyType.click();
        selectByTextInDropDownMenu(menu_companyType, companyType);
    }

    public TerritorySearchPage setTerritory (String territory) throws InterruptedException {
        field_territory.sendKeys(territory);
        clickAndWaitForText(btn_search, TerritorySearchPage.title_territory, TerritorySearchPage.TITLE_PAGE_TEXT);
        return new TerritorySearchPage(driver);
    }

    public void setStreetType (String streetType) throws InterruptedException {
        menu_streetType.click();
        selectByTextInDropDownMenu(menu_streetType, streetType);
        Thread.sleep(4000);
    }

    public void  setStreetName (String streetName) throws InterruptedException {
        field_street.sendKeys(streetName);
    }

    public void setBuildingNumber (String buildingNumber) throws InterruptedException {
        field_building.sendKeys(buildingNumber);
    }

    public void clickBtnSave() throws InterruptedException {
        btn_save.click();
        Thread.sleep(4000);
    }

    public CompanyContactPage clickBtnBack () throws InterruptedException {
        clickAndWaitForText(btn_backCompanyForm, CompanyContactPage.title_CompanyContact, CompanyContactPage.TITLE_PAGE_TEXT);
        return new CompanyContactPage(driver);
    }
}
