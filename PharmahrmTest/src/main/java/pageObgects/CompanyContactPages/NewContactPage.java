package pageObgects.CompanyContactPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import pageObgects.Page;


public class NewContactPage extends Page{

    //Title
    public static final String NEW_CONTACT_TITLE_TEXT = "Новый клиент";

    @FindBy (how = How.CSS, using = "#contactform > div.ui-header.ui-bar-a > h1.ui-title")
    public static WebElement title_newContact;

    //Field of page

    @FindBy (how = How.ID, using = "contactform_lastname")
    public static WebElement field_lastName;

    @FindBy (how = How.ID, using = "contactform_firstname")
    public static WebElement field_FirstName;

    @FindBy (how = How.ID, using = "contactform_middlename")
    public static WebElement field_MiddleName;

    @FindBy (how = How.ID, using = "contactform_companyname")
    public static WebElement field_CompanyName;

    // Drop-down menu of company form

    @FindBy (how = How.ID, using = "contactform_specialization_id")
    public static WebElement menu_specialization;

    @FindBy (how = How.ID, using = "contactform_contacttype_id")
    public static WebElement menu_contactTyp;

    //Button of company form

    @FindBy (how = How.ID, using = "contactform_searchcompany")
    public static WebElement btn_searchCompany;

    @FindBy (how = How.XPATH, using = "//div[@id='contactform']/div[2]/div[20]/div/span")
    public static WebElement btn_save;

    @FindBy (how = How.XPATH, using = "//div[@id='contactform']/div/a/span")
    public static WebElement btn_back;


    public NewContactPage(WebDriver driver) {
        super(driver);
    }

    public void setClientLastName(String newContactLastName) {
        field_lastName.sendKeys(newContactLastName);
    }

    public void setContactFirstName(String newContactFirstName) {
        field_FirstName.sendKeys(newContactFirstName);
    }

    public void setClientMiddleName(String newContactMiddleName) {
        field_MiddleName.sendKeys(newContactMiddleName);
    }

    public void setSpecialization(String contactFormSpecialization) {
        menu_specialization.click();
        selectByTextInDropDownMenu(menu_specialization, contactFormSpecialization);
    }

    public void setContactType(String contactFormContactType) {
        menu_contactTyp.click();
        selectByTextInDropDownMenu(menu_contactTyp, contactFormContactType);
    }

    public CompanySearchPage setCompanyName(String newHospitalName) {
        field_CompanyName.sendKeys(newHospitalName);
        btn_searchCompany.click();
        return new CompanySearchPage(driver);
    }


    public void clickBtnSave() throws InterruptedException {
        btn_save.click();
        Thread.sleep(4000);
    }

    public CompanyContactPage clickBtnBack() throws InterruptedException {
        clickAndWaitForText(btn_back, CompanyContactPage.title_CompanyContact, CompanyContactPage.TITLE_PAGE_TEXT);
        return new CompanyContactPage(driver);
    }
}
