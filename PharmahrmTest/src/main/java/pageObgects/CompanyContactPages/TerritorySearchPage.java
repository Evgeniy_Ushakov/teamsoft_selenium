package pageObgects.CompanyContactPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import pageObgects.Page;

/**
 * Created on 21.08.2014.
 */
public class TerritorySearchPage extends Page {

    public static  String TITLE_PAGE_TEXT = "Населенный пункт";


    // Title

    @FindBy(how = How.CSS, using = "#territory > div.ui-header.ui-bar-a > h1.ui-title")
    public static WebElement title_territory;

    //Button of territories

    @FindBy (how = How.XPATH, using = "//ul[@id='territory_list']/li/div/div/a")
    public static WebElement btn_territory;

  public TerritorySearchPage(WebDriver driver) {
      super(driver);
  }

    public NewCompanyPage chooseTerritory (String newCompanyName) throws InterruptedException {
        String territoryName = btn_territory.getText();
        System.out.println(territoryName);
        clickAndWaitForText(btn_territory, NewCompanyPage.title_companyForm, NewCompanyPage.TITLE_PAGE_TEXT);
        return new NewCompanyPage(driver);
    }
}
