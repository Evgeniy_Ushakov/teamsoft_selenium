package pageObgects.CompanyContactPages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import pageObgects.Page;

public class ContactSearchPage extends Page{

    // title
    public static final String TITLE_PAGE_TEXT = "Клиенты";

    @FindBy(how = How.CSS, using = "#contact > div.ui-header.ui-bar-a > h1.ui-title")
    public static WebElement title_contact;

    // Filed of company page

    @FindBy(how = How.ID, using = "contact_search")
    public static WebElement field_contactSearch;

    // Buttons of company search page

    @FindBy(how = How.ID, using = "contact_search_button")
    public static WebElement btn_search;

    // Company list
    @FindBy(how = How.XPATH, using = "//ul[@id='contact_list']/li/div/div/a/h4")
    public static WebElement firstCompanyInList;

    public ContactSearchPage(WebDriver driver) {
        super(driver);
    }

    public void setFieldValue(String contactSearch) throws InterruptedException {
        field_contactSearch.sendKeys(contactSearch);
        clickAndWaitForText(btn_search, firstCompanyInList, contactSearch);
        saveText(firstCompanyInList);
    }
}
