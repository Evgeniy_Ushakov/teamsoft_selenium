package pageObgects.CompanyContactPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import pageObgects.HomePage;
import pageObgects.Page;


public class CompanyContactPage extends Page {

    public static final String TITLE_PAGE_TEXT = "Учреждения и клиенты";

    @FindBy(how = How.CSS, using = "#company_contact > div.ui-header.ui-bar-a > h1.ui-title")
    public static WebElement title_CompanyContact;

    // Button of page
    @FindBy (how = How.XPATH, using = "//div[@id='company_contact']/div/a[2]/span/span")
    public static WebElement btn_MainCompanyContact;

    // Section of page
    @FindBy (how = How.ID, using = "company_page")
    public static WebElement btn_company;

    @FindBy (how = How.ID, using = "my_company_page")
    public static WebElement btn_myCompany;

    @FindBy (how = How.ID, using = "new_company_page")
    public static WebElement btn_newCompany;

    @FindBy (how = How.ID, using = "company_archive_page")
    public static WebElement btn_companyArchive;

    @FindBy (how = How.ID, using = "contact_page")
    public static WebElement btn_contact;

    @FindBy (how = How.ID, using = "my_contact_page")
    public static WebElement btn_myContact;

    @FindBy (how = How.ID, using = "new_contact_page")
    public static WebElement btn_newContact;

    @FindBy (how = How.ID, using = "contact_archive_page")
    public static WebElement btn_contactArchive;

    public CompanyContactPage (WebDriver driver) {
        super(driver);
    }

    public void clickBtnMain () throws InterruptedException {
        clickAndWaitForText(btn_MainCompanyContact, HomePage.title_main, "Главная");
    }

    public NewCompanyPage clickBtnNewCompany () throws InterruptedException {
        clickAndWaitForText(btn_newCompany, NewCompanyPage.title_companyForm, NewCompanyPage.TITLE_PAGE_TEXT);
        return new NewCompanyPage(driver);
    }

    public CompanySearchPage clickBtnCompany () throws InterruptedException {
        clickAndWaitForText(btn_company, CompanySearchPage.title_newCompany, CompanySearchPage.TITLE_PAGE_TEXT);
        return new CompanySearchPage(driver);
    }

    public NewContactPage clickBtnNewContact() throws InterruptedException {
        clickAndWaitForText(btn_newContact, NewContactPage.title_newContact, NewContactPage.NEW_CONTACT_TITLE_TEXT);
        return new NewContactPage(driver);
    }

    public ContactSearchPage clickBtnContact() throws InterruptedException {
        clickAndWaitForText(btn_contact, ContactSearchPage.title_contact, ContactSearchPage.TITLE_PAGE_TEXT);
        return new ContactSearchPage (driver);
    }
}
