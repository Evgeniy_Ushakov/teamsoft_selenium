package pageObgects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created on 04.07.2014.
 */
public class LogInPagePOM {

    private static WebElement element = null;

    public static WebElement txtbx_UserName (WebDriver driver){
        element = driver.findElement(By.name("login"));
        return element;
    }
    public static WebElement txtbx_Password (WebDriver driver) {
        element = driver.findElement(By.name("password"));
        return  element;
    }
    public static WebElement btn_LogIn (WebDriver driver){
        element = driver.findElement(By.cssSelector("input.ui-btn-hidden"));
        return element;
    }


}
