package pageObgects.ProfilePages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import pageObgects.Page;


public class DatePickerPage extends Page {

    @FindBy (how = How.CSS, using = "span.ui-btn-inner")
    public static WebElement btn_back;

    @FindBy (how = How.XPATH, using = "//div[@id='div_datepicker']/div/table/tbody/tr[5]/td[2]/a")
    public static WebElement datePickerDay;

    @FindBy (how = How.XPATH, using = "//div[@id='div_datepicker']/div/div/a/span")
    public static WebElement btn_leftArrowMonth;

    @FindBy (how = How.XPATH, using = "//div[@id='div_datepicker']/div/div/a[2]/span/span[2]")
    public static WebElement btn_rightArrowMonth;

    @FindBy (how = How.CSS, using = "span.ui-datepicker-month")
    public static WebElement nameOfMonth;

    @FindBy (how = How.XPATH, using = "html/body/div[4]/div[2]/div/div/div/div/div/div/select")
    public static WebElement datePickerYear;


    public DatePickerPage(WebDriver driver) {
        super(driver);
    }

    public void clickDatePickerDay () throws InterruptedException {
                ifElementPresentClickAndWait(datePickerDay);
    }

    public void selectYear () {
        Actions actions = new Actions(driver);
        actions.click(datePickerYear).sendKeys(Keys.ARROW_UP, Keys.ENTER).perform();
    }

    public void clickLeftArrowMonth () throws InterruptedException {
        ifElementPresentClickAndWait(btn_leftArrowMonth);
    }

    public void clickRightArrowMonth () throws InterruptedException {
        ifElementPresentClickAndWait(btn_rightArrowMonth);
    }

    public String saveDatePickerMonth() {
        String month = saveText(nameOfMonth);
        return month;
    }

    public void clickBtnBackOfDatePicker () throws InterruptedException {
        clickAndWaitForText(btn_back, ProfilePage.title_profile, "Профиль");
    }
}
