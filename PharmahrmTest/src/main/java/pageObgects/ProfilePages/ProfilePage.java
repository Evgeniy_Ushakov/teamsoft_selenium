package pageObgects.ProfilePages;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import pageObgects.HomePage;
import pageObgects.Page;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class ProfilePage extends Page {

    @FindBy (how = How.CSS, using = "#profile > div.ui-header.ui-bar-a > h1.ui-title")
    public static WebElement title_profile;
    @FindBy (how = How.XPATH, using = "//div[@id='profile']/div/a/span/span")
    public static WebElement btn_Back;
    @FindBy (how = How.XPATH, using = "//div[@id='profile']/div/a[2]/span/span")
    public static WebElement btn_Main;

     // Date
    @FindBy (how = How.XPATH, using = "//div[2]/div/a[2]/span/span[2]")
    public static WebElement btn_RightArrowDate;
    @FindBy (how = How.XPATH, using = "//div[2]/div/a/span/span[2]")
    public static WebElement btn_LeftArrowDate;
    @FindBy (how = How.ID, using = "profile_date")
    public static WebElement profileDate;


    // New activity
    @FindBy (how = How.XPATH, using = "//div[7]/div[2]/div[2]/div/div/div/span/span") public static WebElement btn_NewActivity;

    // Currently location
    @FindBy (how = How.XPATH, using = "//div[7]/div[2]/div[5]/div/div[4]/span/span") public static WebElement btn_CurrentlyLocation;

    public ProfilePage (WebDriver driver) {
        super(driver);
    }

    public void clickBtnMain () throws InterruptedException {
        clickAndWaitForText(btn_Main, HomePage.title_main, "Главная");
    }

    public void clickBtnBack() throws InterruptedException {
        clickAndWaitForText(btn_Back, HomePage.title_main, "Главная");
    }

    public String saveProfileDate(){
       String date = saveAttribute(profileDate);
        return date;
    }

        public void clickProfileDate() throws InterruptedException {
            Thread.sleep(1000);
            assertTrue(isElementPresent(profileDate));
            profileDate.click();
            assertTrue(isElementPresent(By.cssSelector("span.ui-datepicker-month")));
        }

    public void clickBtnLeftArrowDate () throws InterruptedException {
        Thread.sleep(1000);
        for (int second = 0; ; second++) {
            if (second >= 10) fail("timeout");
            try {
                if (isElementPresent(btn_LeftArrowDate)) break;
            } catch (Exception e) {
            }
            Thread.sleep(1000);
        }
        assertTrue(isElementPresent(btn_LeftArrowDate));
        btn_LeftArrowDate.click();
    }

    public void clickBtnRightArrowDate () throws InterruptedException {
        Thread.sleep(1000);
        for (int second = 0; ; second++) {
            if (second >= 10) fail("timeout");
            try {
                if (isElementPresent(btn_RightArrowDate)) break;
            } catch (Exception e) {
            }
            Thread.sleep(1000);
        }
        assertTrue(isElementPresent(btn_RightArrowDate));
        btn_RightArrowDate.click();
    }

}
