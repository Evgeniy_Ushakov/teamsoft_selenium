package com.testNG.pharmahrm;

import com.thoughtworks.selenium.SeleneseTestNgHelper;
import org.testng.annotations.Test;

public class Exit extends SeleneseTestNgHelper {
        @Test public void Exit() throws Exception {
            selenium.open("/index.php/mobile/c_main");
            selenium.waitForPageToLoad("15000");
            selenium.click("//div[@id='main']/div[2]/ul/li[10]/div/div/a/h3");
            selenium.waitForPageToLoad("15000");
            for (int second = 0;; second++) {
                if (second >= 60) fail("timeout");
                try { if (selenium.isElementPresent("css=h1.ui-title")) break; } catch (Exception e) {}
                Thread.sleep(1000);
            }

            verifyTrue(selenium.isElementPresent("css=h1.ui-title"));
        }
    }