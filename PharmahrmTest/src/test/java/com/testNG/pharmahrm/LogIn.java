package com.testNG.pharmahrm;

import com.thoughtworks.selenium.SeleneseTestNgHelper;
import org.testng.annotations.Test;

public class LogIn extends SeleneseTestNgHelper {
        @Test public void Login() throws Exception {
            selenium.open("http://test.pharmahrm.com/test/");
            selenium.waitForPageToLoad("15000");
            selenium.type( "name=login", "admin");
            selenium.type("name=password", "test090");
            selenium.click("css=input.ui-btn-hidden");
            selenium.waitForPageToLoad("15000");
            for (int second = 0;; second++) {
                if (second >= 60) fail("timeout");
                try { if (selenium.isElementPresent("css=div.ui-header.ui-bar-a")) break; } catch (Exception e) {}
                Thread.sleep(1000);
            }

            assertEquals(selenium.getText("css=h1.ui-title"), "Главная");
        }
    }

