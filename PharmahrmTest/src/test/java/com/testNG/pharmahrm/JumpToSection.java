package com.testNG.pharmahrm;

import com.thoughtworks.selenium.SeleneseTestNgHelper;
import org.testng.annotations.Test;

public class JumpToSection extends SeleneseTestNgHelper {
        @Test public void JumpToSection () throws Exception {
            selenium.open("/index.php/mobile/c_main");
            selenium.waitForPageToLoad("15000");
            for (int second = 0;; second++) {
                if (second >= 60) fail("timeout");
                try { if (selenium.isElementPresent("css=div.ui-header.ui-bar-a")) break; } catch (Exception e) {}
                Thread.sleep(1000);
            }

            selenium.click("id=profile_page");
            for (int second = 0;; second++) {
                if (second >= 60) fail("timeout");
                try { if (selenium.isElementPresent("css=#profile > div.ui-header.ui-bar-a")) break; } catch (Exception e) {}
                Thread.sleep(1000);
            }

            verifyEquals(selenium.getText("css=#profile > div.ui-header.ui-bar-a > h1.ui-title"), "Профиль");
            selenium.click("//div[@id='profile']/div/a[2]/span/span");
            for (int second = 0;; second++) {
                if (second >= 60) fail("timeout");
                try { if (selenium.isElementPresent("css=div.ui-header.ui-bar-a")) break; } catch (Exception e) {}
                Thread.sleep(1000);
            }

            verifyEquals(selenium.getText("css=h1.ui-title"), "Главная");
            selenium.click("css=#new_active_page > h3.ui-li-heading");
            for (int second = 0;; second++) {
                if (second >= 60) fail("timeout");
                try { if (selenium.isElementPresent("css=#new_active > div.ui-header.ui-bar-a")) break; } catch (Exception e) {}
                Thread.sleep(1000);
            }

            verifyEquals(selenium.getText("css=#new_active > div.ui-header.ui-bar-a > h1.ui-title"), "Новая активность");
            selenium.click("//div[@id='new_active']/div/a[2]/span");
            verifyTrue(selenium.isElementPresent("css=h1.ui-title"));
            verifyEquals(selenium.getText("css=h1.ui-title"), "Главная");
            selenium.click("id=company_contact_page");
            for (int second = 0;; second++) {
                if (second >= 60) fail("timeout");
                try { if (selenium.isElementPresent("css=#company_contact > div.ui-header.ui-bar-a > h1.ui-title")) break; } catch (Exception e) {}
                Thread.sleep(1000);
            }

            verifyEquals(selenium.getText("css=#company_contact > div.ui-header.ui-bar-a > h1.ui-title"), "Учреждения и клиенты");
            selenium.click("//div[@id='company_contact']/div/a[2]/span/span");
            for (int second = 0;; second++) {
                if (second >= 60) fail("timeout");
                try { if (selenium.isElementPresent("css=h1.ui-title")) break; } catch (Exception e) {}
                Thread.sleep(1000);
            }

            verifyEquals(selenium.getText("css=h1.ui-title"), "Главная");
            selenium.click("id=report_page");
            for (int second = 0;; second++) {
                if (second >= 60) fail("timeout");
                try { if (selenium.isElementPresent("css=#report > div.ui-header.ui-bar-a")) break; } catch (Exception e) {}
                Thread.sleep(1000);
            }

            verifyEquals(selenium.getText("css=#report > div.ui-header.ui-bar-a > h1.ui-title"), "Отчеты");
            selenium.click("//div[@id='report']/div/a[2]/span/span");
            for (int second = 0;; second++) {
                if (second >= 60) fail("timeout");
                try { if (selenium.isElementPresent("css=h1.ui-title")) break; } catch (Exception e) {}
                Thread.sleep(1000);
            }

            verifyEquals(selenium.getText("css=h1.ui-title"), "Главная");
            selenium.click("id=organogramma_page");
            for (int second = 0;; second++) {
                if (second >= 60) fail("timeout");
                try { if (selenium.isElementPresent("css=#organogramma > div.ui-header.ui-bar-a")) break; } catch (Exception e) {}
                Thread.sleep(1000);
            }

            verifyEquals(selenium.getText("css=#organogramma > div.ui-header.ui-bar-a > h1.ui-title"), "Органограмма");
            selenium.click("//div[@id='organogramma']/div/a[2]/span");
            for (int second = 0;; second++) {
                if (second >= 60) fail("timeout");
                try { if (selenium.isElementPresent("css=h1.ui-title")) break; } catch (Exception e) {}
                Thread.sleep(1000);
            }

            verifyEquals(selenium.getText("css=h1.ui-title"), "Главная");
            selenium.click("id=presentation_page");
            for (int second = 0;; second++) {
                if (second >= 60) fail("timeout");
                try { if (selenium.isElementPresent("css=#presentation > div.ui-header.ui-bar-a > h1.ui-title")) break; } catch (Exception e) {}
                Thread.sleep(1000);
            }

            verifyEquals(selenium.getText("css=#presentation > div.ui-header.ui-bar-a > h1.ui-title"), "Презентации");
            selenium.click("//div[@id='presentation']/div/a[2]/span/span");
            for (int second = 0;; second++) {
                if (second >= 60) fail("timeout");
                try { if (selenium.isElementPresent("css=h1.ui-title")) break; } catch (Exception e) {}
                Thread.sleep(1000);
            }

            verifyEquals(selenium.getText("css=h1.ui-title"), "Главная");
            selenium.click("id=forum");
            for (int second = 0;; second++) {
                if (second >= 60) fail("timeout");
                try { if (selenium.isElementPresent("css=#forumsection > div.ui-header.ui-bar-a > h1.ui-title")) break; } catch (Exception e) {}
                Thread.sleep(1000);
            }

            verifyEquals(selenium.getText("css=#forumsection > div.ui-header.ui-bar-a > h1.ui-title"), "Разделы форума");
            selenium.click("//div[@id='forumsection']/div/a[2]/span/span");
            verifyTrue(selenium.isElementPresent("css=h1.ui-title"));
            verifyEquals(selenium.getText("css=h1.ui-title"), "Главная");
            selenium.click("css=#settings_page > h3.ui-li-heading");
            for (int second = 0;; second++) {
                if (second >= 60) fail("timeout");
                try { if (selenium.isElementPresent("css=#settings > div.ui-header.ui-bar-a")) break; } catch (Exception e) {}
                Thread.sleep(1000);
            }

            verifyEquals(selenium.getText("css=#settings > div.ui-header.ui-bar-a > h1.ui-title"), "Настройки");
            selenium.click("//div[@id='settings']/div/a[2]/span/span");
            for (int second = 0;; second++) {
                if (second >= 60) fail("timeout");
                try { if (selenium.isElementPresent("css=h1.ui-title")) break; } catch (Exception e) {}
                Thread.sleep(1000);
            }

            verifyEquals(selenium.getText("css=h1.ui-title"), "Главная");
            selenium.click("id=help_page");
            for (int second = 0;; second++) {
                if (second >= 60) fail("timeout");
                try { if (selenium.isElementPresent("css=#help > div.ui-header.ui-bar-a > h1.ui-title")) break; } catch (Exception e) {}
                Thread.sleep(1000);
            }

            verifyEquals(selenium.getText("css=#help > div.ui-header.ui-bar-a > h1.ui-title"), "Помощь");
            selenium.click("//div[@id='help']/div/a[2]/span");
            for (int second = 0;; second++) {
                if (second >= 60) fail("timeout");
                try { if (selenium.isElementPresent("css=h1.ui-title")) break; } catch (Exception e) {}
                Thread.sleep(1000);
            }

            verifyEquals(selenium.getText("css=h1.ui-title"), "Главная");
        }
    }
