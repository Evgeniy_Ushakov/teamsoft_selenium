package com.example.test;



import org.openqa.selenium.By;
import org.testng.annotations.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;


public class TestCaseExit extends TestBase {


    @Test (groups = {"exit"}, dependsOnGroups = {"section"})
        public void testExit() throws Exception {
            driver.get(baseUrl);
            driver.findElement(By.xpath("//div[@id='main']/div[2]/ul/li[10]/div/div/a/h3")).click();
            for (int second = 0;; second++) {
                if (second >= 60) fail("timeout");
                try { if (isElementPresent(By.cssSelector("h1.ui-title"))) break; } catch (Exception ignored) {}
                Thread.sleep(1000);
            }

            try {
                assertTrue(isElementPresent(By.cssSelector("h1.ui-title")));
            } catch (Error e) {
                verificationErrors.append(e.toString());
            }
        }


    }



