package com.example.test;


import org.openqa.selenium.By;
import org.testng.annotations.Test;

import static org.junit.Assert.*;

public class TestCaseJumpToSection extends TestBase {




    @Test (groups = {"section"}, dependsOnGroups = {"login"})
    public void testJumpToSection() throws Exception {
            driver.get(baseUrl);


            for (int second = 0;; second++) {
                if (second >= 60) fail("timeout");
                try { if (isElementPresent(By.cssSelector("div.ui-header.ui-bar-a"))) break; } catch (Exception ignored) {}
                Thread.sleep(1000);
            }

            driver.findElement(By.id("profile_page")).click();
            for (int second = 0;; second++) {
                if (second >= 60) fail("timeout");
                try { if (isElementPresent(By.cssSelector("#profile > div.ui-header.ui-bar-a"))) break; } catch (Exception ignored) {}
                Thread.sleep(1000);
            }

            try {
                assertEquals("Профиль", driver.findElement(By.cssSelector("#profile > div.ui-header.ui-bar-a > h1.ui-title")).getText());
            } catch (Error e) {
                verificationErrors.append(e.toString());
            }
            driver.findElement(By.xpath("//div[@id='profile']/div/a[2]/span/span")).click();
            for (int second = 0;; second++) {
                if (second >= 60) fail("timeout");
                try { if (isElementPresent(By.cssSelector("div.ui-header.ui-bar-a"))) break; } catch (Exception ignored) {}
                Thread.sleep(1000);
            }

            try {
                assertEquals("Главная", driver.findElement(By.cssSelector("h1.ui-title")).getText());
            } catch (Error e) {
                verificationErrors.append(e.toString());
            }
            driver.findElement(By.cssSelector("#new_active_page > h3.ui-li-heading")).click();
            for (int second = 0;; second++) {
                if (second >= 60) fail("timeout");
                try { if (isElementPresent(By.cssSelector("#new_active > div.ui-header.ui-bar-a"))) break; } catch (Exception ignored) {}
                Thread.sleep(1000);
            }

            try {
                assertEquals("Новая активность", driver.findElement(By.cssSelector("#new_active > div.ui-header.ui-bar-a > h1.ui-title")).getText());
            } catch (Error e) {
                verificationErrors.append(e.toString());
            }
            driver.findElement(By.xpath("//div[@id='new_active']/div/a[2]/span")).click();
            try {
                assertTrue(isElementPresent(By.cssSelector("h1.ui-title")));
            } catch (Error e) {
                verificationErrors.append(e.toString());
            }
            try {
                assertEquals("Главная", driver.findElement(By.cssSelector("h1.ui-title")).getText());
            } catch (Error e) {
                verificationErrors.append(e.toString());
            }
            driver.findElement(By.id("company_contact_page")).click();
            for (int second = 0;; second++) {
                if (second >= 60) fail("timeout");
                try { if (isElementPresent(By.cssSelector("#company_contact > div.ui-header.ui-bar-a > h1.ui-title"))) break; } catch (Exception ignored) {}
                Thread.sleep(1000);
            }

            try {
                assertEquals("Учреждения и клиенты", driver.findElement(By.cssSelector("#company_contact > div.ui-header.ui-bar-a > h1.ui-title")).getText());
            } catch (Error e) {
                verificationErrors.append(e.toString());
            }
            driver.findElement(By.xpath("//div[@id='company_contact']/div/a[2]/span/span")).click();
            for (int second = 0;; second++) {
                if (second >= 60) fail("timeout");
                try { if (isElementPresent(By.cssSelector("h1.ui-title"))) break; } catch (Exception ignored) {}
                Thread.sleep(1000);
            }

            try {
                assertEquals("Главная", driver.findElement(By.cssSelector("h1.ui-title")).getText());
            } catch (Error e) {
                verificationErrors.append(e.toString());
            }
            driver.findElement(By.id("report_page")).click();
            for (int second = 0;; second++) {
                if (second >= 60) fail("timeout");
                try { if (isElementPresent(By.cssSelector("#report > div.ui-header.ui-bar-a"))) break; } catch (Exception ignored) {}
                Thread.sleep(1000);
            }

            try {
                assertEquals("Отчеты", driver.findElement(By.cssSelector("#report > div.ui-header.ui-bar-a > h1.ui-title")).getText());
            } catch (Error e) {
                verificationErrors.append(e.toString());
            }
            driver.findElement(By.xpath("//div[@id='report']/div/a[2]/span/span")).click();
            for (int second = 0;; second++) {
                if (second >= 60) fail("timeout");
                try { if (isElementPresent(By.cssSelector("h1.ui-title"))) break; } catch (Exception ignored) {}
                Thread.sleep(1000);
            }

            try {
                assertEquals("Главная", driver.findElement(By.cssSelector("h1.ui-title")).getText());
            } catch (Error e) {
                verificationErrors.append(e.toString());
            }
            driver.findElement(By.id("organogramma_page")).click();
            for (int second = 0;; second++) {
                if (second >= 60) fail("timeout");
                try { if (isElementPresent(By.cssSelector("#organogramma > div.ui-header.ui-bar-a"))) break; } catch (Exception ignored) {}
                Thread.sleep(1000);
            }

            try {
                assertEquals("Органограмма", driver.findElement(By.cssSelector("#organogramma > div.ui-header.ui-bar-a > h1.ui-title")).getText());
            } catch (Error e) {
                verificationErrors.append(e.toString());
            }
            driver.findElement(By.xpath("//div[@id='organogramma']/div/a[2]/span")).click();
            for (int second = 0;; second++) {
                if (second >= 60) fail("timeout");
                try { if (isElementPresent(By.cssSelector("h1.ui-title"))) break; } catch (Exception ignored) {}
                Thread.sleep(1000);
            }

            try {
                assertEquals("Главная", driver.findElement(By.cssSelector("h1.ui-title")).getText());
            } catch (Error e) {
                verificationErrors.append(e.toString());
            }
            driver.findElement(By.id("presentation_page")).click();
            for (int second = 0;; second++) {
                if (second >= 60) fail("timeout");
                try { if (isElementPresent(By.cssSelector("#presentation > div.ui-header.ui-bar-a > h1.ui-title"))) break; } catch (Exception ignored) {}
                Thread.sleep(1000);
            }

            try {
                assertEquals("Презентации", driver.findElement(By.cssSelector("#presentation > div.ui-header.ui-bar-a > h1.ui-title")).getText());
            } catch (Error e) {
                verificationErrors.append(e.toString());
            }
            driver.findElement(By.xpath("//div[@id='presentation']/div/a[2]/span/span")).click();
            for (int second = 0;; second++) {
                if (second >= 60) fail("timeout");
                try { if (isElementPresent(By.cssSelector("h1.ui-title"))) break; } catch (Exception ignored) {}
                Thread.sleep(1000);
            }

            try {
                assertEquals("Главная", driver.findElement(By.cssSelector("h1.ui-title")).getText());
            } catch (Error e) {
                verificationErrors.append(e.toString());
            }
            driver.findElement(By.id("forum")).click();
            for (int second = 0;; second++) {
                if (second >= 60) fail("timeout");
                try { if (isElementPresent(By.cssSelector("#forumsection > div.ui-header.ui-bar-a > h1.ui-title"))) break; } catch (Exception ignored) {}
                Thread.sleep(1000);
            }

            try {
                assertEquals("Разделы форума", driver.findElement(By.cssSelector("#forumsection > div.ui-header.ui-bar-a > h1.ui-title")).getText());
            } catch (Error e) {
                verificationErrors.append(e.toString());
            }
            driver.findElement(By.xpath("//div[@id='forumsection']/div/a[2]/span/span")).click();
            try {
                assertTrue(isElementPresent(By.cssSelector("h1.ui-title")));
            } catch (Error e) {
                verificationErrors.append(e.toString());
            }
            try {
                assertEquals("Главная", driver.findElement(By.cssSelector("h1.ui-title")).getText());
            } catch (Error e) {
                verificationErrors.append(e.toString());
            }
            driver.findElement(By.cssSelector("#settings_page > h3.ui-li-heading")).click();
            for (int second = 0;; second++) {
                if (second >= 60) fail("timeout");
                try { if (isElementPresent(By.cssSelector("#settings > div.ui-header.ui-bar-a"))) break; } catch (Exception ignored) {}
                Thread.sleep(1000);
            }

            try {
                assertEquals("Настройки", driver.findElement(By.cssSelector("#settings > div.ui-header.ui-bar-a > h1.ui-title")).getText());
            } catch (Error e) {
                verificationErrors.append(e.toString());
            }
            driver.findElement(By.xpath("//div[@id='settings']/div/a[2]/span/span")).click();
            for (int second = 0;; second++) {
                if (second >= 60) fail("timeout");
                try { if (isElementPresent(By.cssSelector("h1.ui-title"))) break; } catch (Exception ignored) {}
                Thread.sleep(1000);
            }

            try {
                assertEquals("Главная", driver.findElement(By.cssSelector("h1.ui-title")).getText());
            } catch (Error e) {
                verificationErrors.append(e.toString());
            }
            driver.findElement(By.id("help_page")).click();
            for (int second = 0;; second++) {
                if (second >= 60) fail("timeout");
                try { if (isElementPresent(By.cssSelector("#help > div.ui-header.ui-bar-a > h1.ui-title"))) break; } catch (Exception ignored) {}
                Thread.sleep(1000);
            }

            try {
                assertEquals("Помощь", driver.findElement(By.cssSelector("#help > div.ui-header.ui-bar-a > h1.ui-title")).getText());
            } catch (Error e) {
                verificationErrors.append(e.toString());
            }
            driver.findElement(By.xpath("//div[@id='help']/div/a[2]/span")).click();
            for (int second = 0;; second++) {
                if (second >= 60) fail("timeout");
                try { if (isElementPresent(By.cssSelector("h1.ui-title"))) break; } catch (Exception ignored) {}
                Thread.sleep(1000);
            }

            try {
                assertEquals("Главная", driver.findElement(By.cssSelector("h1.ui-title")).getText());
            } catch (Error e) {
                verificationErrors.append(e.toString());
            }
        }

    }


