package com.example.test;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.fail;

public class TestBase {
    public static String baseUrl;
    public static StringBuffer verificationErrors = new StringBuffer();
    protected static WebDriver driver;

    @BeforeSuite
    public static void setUp() throws Exception {
            driver = new FirefoxDriver();
         //   baseUrl = "http://arterium.pharmahrm.com";
         //   baseUrl = "http://naturprodukt.pharmahrm.com/";     //+
          baseUrl = "http://vishpha.pharmahrm.com/";
        //    baseUrl = "http://dermapharm.pharmahrm.com/";        //+
       //     baseUrl = "http://farmak.pharmahrm.com/";
      //     baseUrl = "http://test.pharmahrm.com/test/";      //+
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

    }


    @AfterSuite
    public static void tearDown() throws Exception {
            driver.quit();
            String verificationErrorString = verificationErrors.toString();
            if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
            }
            }

    protected boolean isElementPresent(By by) {
            try {
            driver.findElement(by);
            return true;
            } catch (NoSuchElementException e) {
            return false;
            }
            }

    @DataProvider(name = "loginProvider")

    public Iterator<Object[]> createData() throws Exception{
        List<Object[]> list = new ArrayList<Object[]>();
        for (int i = 0; i<1; i++)
            list.add(new Object[]{
                    //http://naturprodukt.pharmahrm.com/
                    //http://vishpha.pharmahrm.com/
                      "tester",    "test090",
                    //http://test.pharmahrm.com/test/
                    //http://farmak.pharmahrm.com/
                //    "test",  "test090",
                    //http://dermapharm.pharmahrm.com/
                    //     "opetr",  "555777",
            });
        return list.iterator();
    }

    /*public  Iterator <Object[]> creatGroup ()throws Exception{
        List<Object[]> list = new ArrayList<Object[]>();
        BufferedReader br = new BufferedReader(new FileReader("testdata.txt"));
        String s = br.readLine();
                while (s != null) {
                    String[] parts = s.split(",");
                  //  GroupData group1 = new GroupData();
                    boolean add;
                    add = list.add(new Object[]{parts});
                    s = br.readLine();
                }
        return  list.iterator();
    }*/

}
