package com.example.test;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class TestCaseLogIn extends TestBase{



    @Test (groups = {"login"}, dataProvider = "loginProvider" )
    public void testLogIn(String login, String password) throws Exception {

        driver.get(baseUrl);
        driver.findElement(By.name("login")).clear();
        driver.findElement(By.name("login")).sendKeys(login);
        driver.findElement(By.name("password")).clear();
        driver.findElement(By.name("password")).sendKeys(password);
        driver.findElement(By.cssSelector("input.ui-btn-hidden")).click();
        for (int second = 0;; second++) {
        if (second >= 60) fail("timeout");
        try { if (isElementPresent(By.cssSelector("div.ui-header.ui-bar-a"))) break; } catch (Exception ignored) {}
        Thread.sleep(1000);
        }

        assertEquals("Главная", driver.findElement(By.cssSelector("h1.ui-title")).getText());
        }



    }


