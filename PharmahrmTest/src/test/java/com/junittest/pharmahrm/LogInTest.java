package com.junittest.pharmahrm;



import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import utility.Constant;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;


@RunWith(DataProviderRunner.class)
public class LogInTest extends TestBase{

    @Test

    public void test1 /* LogIn */() throws Exception {
        driver.get(Constant.URL);

        logInPage.logInSuccess(Constant.USERNAME, Constant.PASSWORD);


        for (int second = 0; ; second++) {
            if (second >= 60) fail("timeout");
            try {
                if (isElementPresent(By.cssSelector("div.ui-header.ui-bar-a"))) break;
            } catch (Exception e) {
            }
            Thread.sleep(1000);
        }

        assertEquals("Главная", driver.findElement(By.cssSelector("h1.ui-title")).getText());
    }


}
