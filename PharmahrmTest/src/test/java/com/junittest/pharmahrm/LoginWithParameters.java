package com.junittest.pharmahrm;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.openqa.selenium.By;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;


@RunWith(Parameterized.class)
public class LoginWithParameters extends TestBase{


        @Test
        @Parameterized.Parameters
        public void test1LogIn(String login, String password) throws Exception {
            driver.get(baseUrl);
            driver.findElement(By.name("login")).clear();
            driver.findElement(By.name("login")).sendKeys(login);
            driver.findElement(By.name("password")).clear();
            driver.findElement(By.name("password")).sendKeys(password);
            driver.findElement(By.cssSelector("input.ui-btn-hidden")).click();
            for (int second = 0; ; second++) {
                if (second >= 60) fail("timeout");
                try {
                    if (isElementPresent(By.cssSelector("div.ui-header.ui-bar-a"))) break;
                } catch (Exception e) {
                }
                Thread.sleep(1000);
            }

            assertEquals("Главная", driver.findElement(By.cssSelector("h1.ui-title")).getText());
        }


    }





