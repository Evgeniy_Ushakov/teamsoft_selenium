package com.junittest.pharmahrm;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import pageObgects.CompanyContactPages.*;
import pageObgects.*;
import pageObgects.ProfilePages.DatePickerPage;
import pageObgects.ProfilePages.ProfilePage;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.fail;


public class TestBase {
    public static WebDriver driver;
    public static String baseUrl;
    public static StringBuffer verificationErrors = new StringBuffer();

    public LogInPage logInPage;
        protected HomePage homePage;
        protected ProfilePage profilePage;
            protected DatePickerPage datePickerPage;
        protected NewActivePage newActivePage;
        protected CompanyContactPage companyContactPage;
            protected CompanySearchPage companySearchPage;
            protected ContactSearchPage contactSearchPage;
            protected NewCompanyPage newCompanyPage;
                protected TerritorySearchPage territorySearchPage;
            protected NewContactPage newContactPage;
        protected ReportPage reportPage;
        protected OrganogrammaPage organogrammaPage;
        protected PresentationPage presentationPage;
        protected ForumPage forumPage;
        protected SettingsPage settingsPage;
        protected HelpPage helpPage;




    @BeforeClass
    @UseDataProvider(value = "dataProviderLoginAndPassword")
       public static void setUp() throws Exception {
        driver = new FirefoxDriver();

        //   baseUrl = "http://arterium.pharmahrm.com";
        //   baseUrl = "http://naturprodukt.pharmahrm.com/";     //+
        //   baseUrl = "http://vishpha.pharmahrm.com/";
        //   baseUrl = "http://dermapharm.pharmahrm.com/";        //+
        //   baseUrl = "http://farmak.pharmahrm.com/";
           baseUrl ="http://test.pharmahrm.com/test/";

        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);


    }
    @Before
    public void initPages (){
        logInPage = PageFactory.initElements(driver, LogInPage.class);
            homePage = PageFactory.initElements(driver, HomePage.class);
            profilePage = PageFactory.initElements(driver, ProfilePage.class);
                datePickerPage = PageFactory.initElements(driver, DatePickerPage.class);
            newActivePage = PageFactory.initElements(driver, NewActivePage.class);
            companyContactPage = PageFactory.initElements(driver, CompanyContactPage.class);
                companySearchPage = PageFactory.initElements(driver, CompanySearchPage.class);
                contactSearchPage = PageFactory.initElements(driver, ContactSearchPage.class);
                newCompanyPage = PageFactory.initElements(driver, NewCompanyPage.class);
                    territorySearchPage = PageFactory.initElements(driver, TerritorySearchPage.class);
                newContactPage = PageFactory.initElements(driver, NewContactPage.class);
            reportPage = PageFactory.initElements(driver, ReportPage.class);
            organogrammaPage = PageFactory.initElements(driver, OrganogrammaPage.class);
            presentationPage = PageFactory.initElements(driver, PresentationPage.class);
            forumPage = PageFactory.initElements(driver, ForumPage.class);
            settingsPage = PageFactory.initElements(driver, SettingsPage.class);
            helpPage = PageFactory.initElements(driver, HelpPage.class);


    }


    @DataProvider
    public static Object[][] dataProviderLoginAndPassword() {
        // @formatter:off
        return new Object[][] {
                //http://naturprodukt.pharmahrm.com/
                //http://vishpha.pharmahrm.com/

               // {"tester",    "test090",}

                //http://test.pharmahrm.com/test/
                //http://farmak.pharmahrm.com/

                    {"test",  "test090"},

                //http://dermapharm.pharmahrm.com/

                //     {"opetr",  "555777"},

        };
        // @formatter:on
    }

    @AfterClass
    public static void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }
    @After
    public void init (){

    }
    public boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public boolean isElementPresent(WebElement element) {
        try {
            element.isDisplayed();
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Element not found");
            return false;
        }

    }

}

