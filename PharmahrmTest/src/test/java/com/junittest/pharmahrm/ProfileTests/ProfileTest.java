package com.junittest.pharmahrm.ProfileTests;

import com.junittest.pharmahrm.TestBase;
import org.junit.Test;
import utility.Constant;

import static org.junit.Assert.assertEquals;

/**
 * List "Профиль" test case #5 in excel
 */
public class ProfileTest extends TestBase {

    @Test
    public void testRealDate () throws Exception {
        driver.get(Constant.URL);

        // STEP 1 ** Log in
        logInPage.logInSuccess(Constant.USERNAME, Constant.PASSWORD);
        // Step 2 ** Go in Profile section and save the date
        homePage.clickBtnProfile();
        String profileDate = profilePage.saveProfileDate();
        // Get real date and compare both dates
        Constant date = new Constant();
        String realDate = date.getDate();
        assertEquals(realDate, profileDate);
    }
}
