package com.junittest.pharmahrm.ProfileTests;

import com.junittest.pharmahrm.TestBase;
import org.junit.Test;
import utility.Constant;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created on 04.08.2014.
 */
public class DatePickerTests extends TestBase {
    /**
     * List "Профиль", test cases #7.1; 7.2 in excel
     * @throws Exception
     */

    @Test
    public void testDatePicker () throws Exception {
        driver.get(Constant.URL);

        // STEP 1 ** Log in
        logInPage.logInSuccess(Constant.USERNAME, Constant.PASSWORD);

        // STEP 2 ** Go to profile page
        homePage.clickBtnProfile();
        //Save the data
        String date1 = profilePage.saveProfileDate();

        //STEP 3 ** Open the picker date
        profilePage.clickProfileDate();

        // STEP 4 ** Choose a day
        datePickerPage.clickDatePickerDay();
        // Save the data
        String date2 = profilePage.saveProfileDate();
        assertNotEquals(date1, date2);

        //Step 5 ** Open the picker date
        profilePage.clickProfileDate();

        //Step 6 ** Choose a year
        datePickerPage.selectYear();

        // STEP 7 ** Choose a day
        datePickerPage.clickDatePickerDay();
        // Save the data
        String date3 = profilePage.saveProfileDate();
        assertNotEquals(date2, date3);

        //Step 8 ** Open the picker date
        profilePage.clickProfileDate();

        // Step 9 ** Select previous month
        String month = datePickerPage.saveDatePickerMonth();
        datePickerPage.clickLeftArrowMonth();
        String previousMonth = datePickerPage.saveDatePickerMonth();
        assertNotEquals(month, previousMonth);

        // STEP 10 ** Choose a day
        datePickerPage.clickDatePickerDay();
        // Save the data
        String date4 = profilePage.saveProfileDate();
        assertNotEquals(date3, date4);

        //Step 11 ** Open the picker date
        profilePage.clickProfileDate();

        // Step 12 ** Select next month
        datePickerPage.clickRightArrowMonth();
        String nextMonth = datePickerPage.saveDatePickerMonth();
        assertNotEquals(previousMonth, nextMonth);

        // STEP 10 ** Choose a day
        datePickerPage.clickDatePickerDay();
        // Save the data
        String date5 = profilePage.saveProfileDate();
        assertNotEquals(date4, date5);
    }

    /**
     * List "Профиль" test cases #7.3; 7.4 in excel
     */
    @Test
    public void testBtnBack ()throws Exception{
        driver.get(Constant.URL);

        // STEP 1 ** Log in
        logInPage.logInSuccess(Constant.USERNAME, Constant.PASSWORD);

        // STEP 2 ** Go to profile page and save the date
        homePage.clickBtnProfile();
        String date1 = profilePage.saveProfileDate();

        //Step 3 ** Open the picker date
        profilePage.clickProfileDate();

        //Step 4 ** Choose a year
        datePickerPage.selectYear();

        // STEP 5 ** Click button back and save date
        datePickerPage.clickBtnBackOfDatePicker();
        String date2 = profilePage.saveProfileDate();
        assertEquals(date1, date2);

        //Step 6 ** Open the picker date
        profilePage.clickProfileDate();

        // Step 7 ** Select previous month
        datePickerPage.clickLeftArrowMonth();

        // STEP 8 ** Click button back and save date
        datePickerPage.clickBtnBackOfDatePicker();
        String date3 = profilePage.saveProfileDate();
        assertEquals(date1, date3);

        //Step 9 ** Open the picker date
        profilePage.clickProfileDate();

        // Step 10 ** Select next month
        datePickerPage.clickRightArrowMonth();

        // STEP 11 ** Click button back and save date
        datePickerPage.clickBtnBackOfDatePicker();
        String date4 = profilePage.saveProfileDate();
        assertEquals(date1, date4);
    }
}
