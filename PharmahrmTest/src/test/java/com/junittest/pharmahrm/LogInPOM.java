package com.junittest.pharmahrm;

/**
 * Created by Роман on 08.07.2014.
 */

import appModul.LogInAction;
import org.junit.Test;
import org.openqa.selenium.By;
import utility.Constant;
import utility.ExcelUtils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;



public class LogInPOM extends TestBase{

    @Test
     public void LogIn() throws Exception {
        driver.get(Constant.URL);

        // Use page Object library
        ExcelUtils.setExcelFile(Constant.PATH_TEST_DATA + Constant.FILE_TEST_DATA, "Sheet1");
        LogInAction.Execute(driver);

        for (int second = 0; ; second++) {
            if (second >= 60) fail("timeout");
            try {
                if (isElementPresent(By.cssSelector("div.ui-header.ui-bar-a"))) break;
            } catch (Exception e) {
            }
            Thread.sleep(1000);
        }

        assertEquals("Главная", driver.findElement(By.cssSelector("h1.ui-title")).getText());

        ExcelUtils.setCellData("Pass", 1, 3);


    }

}

