package com.junittest.pharmahrm;


import org.junit.Test;
import utility.Constant;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;


    public class ProfileTest extends TestBase {
    /*
          *  Test case for buttons "Back", "Main", arrows of date, and profile date
          *  Тест кейсы для моб.версии.xls => Профиль => №4, 6
         */
        @Test
         public void testBtnBackMain() throws Exception {
            driver.get(Constant.URL);

            // Step 1 ** Log in
            logInPage.logInSuccess(Constant.USERNAME, Constant.PASSWORD);

            // STEP 2 ** Go to profile page
            homePage.clickBtnProfile();

            // STEP 3 ** Save the date
            String date1 = profilePage.saveProfileDate();

            // Click on right arrow of date
            profilePage.clickBtnRightArrowDate();

            // Save the date
            String date2 = profilePage.saveProfileDate();
            assertNotEquals(date2, date1);

            // STEP 4 ** Go to main page
            profilePage.clickBtnBack();

            // STEP 5 ** Go to profile page
            homePage.clickBtnProfile();

            // Save the date
            String date3 = profilePage.saveProfileDate();
            assertEquals(date3, date2);

            // STEP 6 ** Click on left arrow of date
            profilePage.clickBtnLeftArrowDate();

            // Save the date
            String date4 = profilePage.saveProfileDate();
            assertEquals(date4, date1);

            // STEP 7 ** Go to main page
            profilePage.clickBtnMain();

            // STEP 8 ** Go to profile page
            homePage.clickBtnProfile();

            // Save the data
            String date5 = profilePage.saveProfileDate();
            assertEquals(date5, date4);
        }
    }