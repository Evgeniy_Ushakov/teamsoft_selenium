package com.junittest.pharmahrm;

import org.junit.Test;
import utility.Constant;

public class JumpToSection extends TestBase{

        @Test
        public void testJumpToSectionTemp() throws Exception {
            driver.get(Constant.URL);
            logInPage.logInSuccess(Constant.USERNAME, Constant.PASSWORD);
            homePage.clickBtnProfile();
            profilePage.clickBtnMain();
            homePage.clickBtnNewActive();
            newActivePage.clickBtnMain();
            homePage.clickBtnCompanyContact();
            companyContactPage.clickBtnMain();
            homePage.clickBtnReport();
            reportPage.clickBtnMain();
            homePage.clickBtnOrganogramma();
            organogrammaPage.clickBtnMain();
            homePage.clickBtnPresentation();
            presentationPage.clickBtnMain();
            homePage.clickBtnForum();
            forumPage.clickBtnMain();
            homePage.clickBtnSettings();
            settingsPage.clickBtnMain();
            homePage.clickBtnHelp();
            helpPage.clickBtnMain();
            homePage.clickBtnExit();
        }
    }
