package com.junittest.pharmahrm.CompanyContactTests;


import com.junittest.pharmahrm.TestBase;
import org.junit.Test;
import utility.Constant;

public class NewContactTest extends TestBase{

    @Test
    public void newClient () throws Exception {
        driver.get(Constant.URL);
        logInPage.logInSuccess(Constant.USERNAME, Constant.PASSWORD);
        homePage.clickBtnCompanyContact();
        companyContactPage.clickBtnNewContact();
        newContactPage.setClientLastName(Constant.NEW_CONTACT_LAST_NAME);
        newContactPage.setContactFirstName(Constant.NEW_CONTACT_NAME);
        newContactPage.setClientMiddleName(Constant.NEW_CONTACT_MIDDLE_NAME);
        newContactPage.setSpecialization(Constant.CONTACT_FORM_SPECIALIZATION);
        newContactPage.setContactType(Constant.CONTACT_FORM_CONTACT_TYPE);
        newContactPage.setCompanyName(Constant.NEW_HOSPITAL_NAME);
        companySearchPage.chooseCompany();
        newContactPage.clickBtnSave();
        newContactPage.clickBtnBack();
        companyContactPage.clickBtnContact();
        contactSearchPage.setFieldValue(Constant.NEW_CONTACT_LAST_NAME + " " + Constant.NEW_CONTACT_NAME + " " + Constant.NEW_CONTACT_MIDDLE_NAME);

    }
}
