package com.junittest.pharmahrm.CompanyContactTests;

import com.junittest.pharmahrm.TestBase;
import org.junit.Test;
import utility.Constant;

public class NewCompanyTest extends TestBase {

    @Test
    public void createNewPharmacy() throws Exception {
        driver.get(Constant.URL);
        logInPage.logInSuccess(Constant.USERNAME, Constant.PASSWORD);
        homePage.clickBtnCompanyContact();
        companyContactPage.clickBtnNewCompany();
        newCompanyPage.setCompanyName(Constant.NEW_PHARMACY_NAME);
        newCompanyPage.setCompanyType(Constant.COMPANY_TYPE);
        newCompanyPage.setTerritory(Constant.TERRITORY);
        territorySearchPage.chooseTerritory(Constant.NEW_PHARMACY_NAME);
        newCompanyPage.setStreetType(Constant.STREET_TYPE);
        newCompanyPage.setStreetName(Constant.STREET_NAME);
        newCompanyPage.setBuildingNumber(Constant.BUILDING_NUMBER);
        newCompanyPage.clickBtnSave();
        newCompanyPage.clickBtnBack();
        companyContactPage.clickBtnCompany();
        companySearchPage.setFieldValue(Constant.NEW_PHARMACY_NAME + " " + Constant.STREET_NAME + " " + Constant.BUILDING_NUMBER + " " + Constant.TERRITORY);
    }

    @Test
    public void createNewHospital () throws Exception {
        driver.get(Constant.URL);
        logInPage.logInSuccess(Constant.USERNAME, Constant.PASSWORD);
        homePage.clickBtnCompanyContact();
        companyContactPage.clickBtnNewCompany();
        newCompanyPage.setCompanyName(Constant.NEW_HOSPITAL_NAME);
        newCompanyPage.setCompanyType(Constant.COMPANY_TYPE);
        newCompanyPage.setTerritory(Constant.TERRITORY);
        territorySearchPage.chooseTerritory(Constant.NEW_HOSPITAL_NAME);
        newCompanyPage.setStreetType(Constant.STREET_TYPE);
        newCompanyPage.setStreetName(Constant.STREET_NAME);
        newCompanyPage.setBuildingNumber(Constant.BUILDING_NUMBER);
        newCompanyPage.clickBtnSave();
        newCompanyPage.clickBtnBack();
        companyContactPage.clickBtnCompany();
        companySearchPage.setFieldValue(Constant.NEW_HOSPITAL_NAME + " " + Constant.STREET_NAME + " " + Constant.BUILDING_NUMBER + " " + Constant.TERRITORY);


    }

}
